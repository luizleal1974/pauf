<div align="justify">

# PAUF

<i>Probabilistic Analysis of Ultrasonic Flowmeter</i> (<b>PAUF</b>) is a friendly interactive web application to evaluate the accuracy of ultrasonic gas flow meters through the theory of structural reliability. It is a <a target='_blank' rel='noopener noreferrer' href='https://shiny.posit.co/' title='Easy web apps for data science without the compromises.'><b>Shiny</b></a> web application framework for <a target='_blank' rel='noopener noreferrer' href='https://www.r-project.org/' title='R is a free software environment for statistical computing and graphics.'><b>R</b></a>. PAUF web application was developed by <a target='_blank' rel='noopener noreferrer' href='http://lattes.cnpq.br/8849370590932263' title='Shiny/R Developer'><b>Leal</b></a>, <a target='_blank' rel='noopener noreferrer' href='http://lattes.cnpq.br/1736380039742054' title='VBA developer'><b>Raunyr</b></a> and <a target='_blank' rel='noopener noreferrer' href='http://lattes.cnpq.br/1030306192969513' title='chemometrician'><b>Rocha</b></a>. It is available freely, under the developer’s <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/pauf/blob/main/DISCLAIMER.md'>`license`</a>, on web site <b>https://statisticalmetrology.shinyapps.io/pauftool/</b>. For any comments or questions, please send an e-mail to <b>statistical.metrology@gmail.com</b>.

<p> <br /> <p>

# Funding / Support

This sofware was funded by Brazilian Petroleum Corporation (<a target='_blank' rel='noopener noreferrer' href='https://petrobras.com.br/en/' title='Brazilian Petroleum Corporation'><b>Petrobras</b></a>) and supported by Federal University of Minas Gerais Support Foundation (<a target='_blank' rel='noopener noreferrer' href='https://www.fundep.ufmg.br/' title='UFMG Support Foundation'><b>Fundep</b></a>).

<p> <br /> <p>

# Research project

This web application (Figure 1) is part of a research project to develop a new methodology for calibrating ultrasonic flowmeters for natural gas. The proposed project aims to metrologically evaluate alternative methods for calibrating natural gas flowmeters, using ultrasonic technology, within the scope of the <a target='_blank' rel='noopener noreferrer' href='https://www.gov.br/anp/pt-br' title='Brazilian National Agency for Petroleum, Natural Gas and Biofuels'><b>ANP</b></a>-<a target='_blank' rel='noopener noreferrer' href='https://www.gov.br/inmetro/pt-br' title='Brazilian National Institute of Metrology, Quality and Technology'><b>Inmetro</b></a> Metrological Technical Regulation, used in fiscal measurement, custody transfer and appropriation operations.

The project evaluates the performance of the meter when calibrated in alternative conditions (calibration fluid, pressure, and temperature) to those of operation. The study consists of a theoretical stage, with survey of influencing factors and preparation of a test matrix, an experimental stage, with tests carried out in several different calibration conditions, and a stage of analysis of results and conclusion.

Additionally, this project contains two feasibility studies on topics of interest to the oil industry. The first of these deals with the possibility of developing and implementing an on-site system for calibrating gas flowmeters, a priori based on sonic nozzle manifolds. The second is an initial study for the use of diagnostic resources, available in modern flowmeters, together with other process parameters, such as flowmeter calibration management and monitoring tools, aiming at the possibility of extending deadlines.

</div>

<div align="center"><img src="app.gif"/></div>

<div align="center">Figure 1. Probabilistic Analysis of Ultrasonic Flowmeter application.</div>

<p> <br /> <p>

<p> <br /> <p>

<div align="justify">

# References

[1] Túlio R.C. Felipe, Luiz H.C. Leal, Fábio O. Costa, Douglas A. Garcia, Werickson F.C. Rocha, A reliability-based framework for comparing ultrasonic flowmeter calibration methods, Flow Measurement and Instrumentation, Volume 102, 2025, 102766. <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/j.flowmeasinst.2024.102766'><img src="https://img.shields.io/badge/doi-10.1016/j.flowmeasinst.2024.102766-yellow.svg" alt="drawing"/></a> <a style="text-decoration:none" target='_blank' rel='noopener noreferrer' href='https://www.sciencedirect.com/journal/flow-measurement-and-instrumentation/vol/102/suppl/C'>`(Volume 102, March 2025)`</a>

</div>

<p> <br /> <p>
